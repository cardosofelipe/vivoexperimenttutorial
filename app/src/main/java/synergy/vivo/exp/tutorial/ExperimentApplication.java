package synergy.vivo.exp.tutorial;
import android.app.Application;
import android.util.Log;
import ch.supsi.netlab.vivoclientapi.VivoService;
import ch.supsi.netlab.vivoclientapi.utils.Callback;
import ch.supsi.netlab.vivoclientapi.utils.CryptoUtils;

public class ExperimentApplication extends Application {
    private static final String TAG = ExperimentApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        /** Crypto conf (mandatory, but you can call this from an activity) **/
        CryptoUtils.setPublicKeyFromAssets(getApplicationContext(), "public_key.der");
        /**
         * Vivo service conf (optional), you can call this from your launcher
         * activity but onStart method won't be called
         * because callback.OnStart() is performed before the activity launch.
         * Please
         **/
        VivoService.setCustomCallback(new Callback() {
            @Override
            public void onStart() {
                Log.d(TAG, "Hello onStart!");
            }
            @Override
            public void onStop() {
                Log.d(TAG, "Hello onStop!");
            }
        });
    }
}