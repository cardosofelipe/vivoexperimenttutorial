package synergy.vivo.exp.tutorial;

import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import ch.supsi.netlab.vivoclientapi.DataSendService;


/**
 * MainActivity è una classe che estende AppCompatActivity
 */
public class MainActivity extends AppCompatActivity {

    /**   executor è un membro di istanza (di MainActivity), qua è solo dichiarata **/
    private ScheduledExecutorService executor;

    /**   DATA_TYPE_FLOAT è un membro di classe  (di MainActivity)  **/
    private static int DATA_TYPE_FLOAT = 0;

    /**
     * onCreate è un metodo (in questo caso, l'override di un metodo della classe madre MainActivity)
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /** qua executor viene istanziata **/
        executor = Executors.newScheduledThreadPool(2);

        /** new Runnable(){ ... } è una anonymous inner class, al suo interno uso DATA_TYPE_FLOAT senza problemi **/
        // Executes every 2 seconds
        executor.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {
                float aFloat = new Random(System.currentTimeMillis()).nextFloat();

                Log.d("EXECUTOR RUN", "data: " + aFloat);
                /** Calling the service for data sending **/
                DataSendService.startActionSendData(MainActivity.this, String.valueOf(aFloat),
                        DATA_TYPE_FLOAT,
                        SystemClock.elapsedRealtime());
            }
        }, 0, 2, TimeUnit.SECONDS);

    }
}
